## Loss landscape visualization

The main goal of this project was to create a setup that can properly visualize some loss landscapes based on the paper [Visualizing the Loss Landscape of Neural Nets](https://arxiv.org/pdf/1712.09913.pdf)

```bash
python -m venv losslandscape
source losslandscape/bin/activate
pip install -r requirements.txt
```

Afterwards to visualize, please download the publicly available `ImagenetV2` test dataset from [here](https://github.com/modestyachts/ImageNetV2).

Having acquired a proper dataset for calculating the loss/accuracy on it you can move on to visualizing the loss landscape of the models:

```bash
python run_imagenet.py --path_to_imagenetv2 <path-to-downloaded-data-root>
```

Afterwards you'll have a loss landscape generated on all models available in PyTorch with pre-trained ImageNet weights. (~ days of processing time)

The `run_log_plot.py` scripts generates the log loss surface and an animation rotating it. :)

-----------------

|    VGG16              |       VGG16-bn         |
|:---------------------:|:----------------------:|
|![vgg16-loss-landscape](results/vgg16_log_surface_imagenetv2.gif) | ![vgg16-bn-loss-landscape](results/vgg16_bn_log_surface_imagenetv2.gif)|

-------

|    **VGG11**             |       **VGG11-bn**         |
|:---------------------:|:----------------------:|
|![vgg11-loss-landscape](results/vgg11_log_surface_imagenetv2.gif) | ![vgg11-bn-loss-landscape](results/vgg11_bn_log_surface_imagenetv2.gif)|

-------

|    **InceptionV3**             |       **Resnet18**         |
|:---------------------:|:----------------------:|
|![inceptionv3-loss-landscape](results/inception_v3_log_surface_imagenetv2.gif) | ![resnet18-bn-loss-landscape](results/resnet18_log_surface_imagenetv2.gif)|
